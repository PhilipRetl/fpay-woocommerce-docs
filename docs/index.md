![Screenshot](img/fpay_logo_short.png)

## Descripción:

EL plugin de [Fpay](https://www.fpay.cl) para [woocommerce](https://woocommerce.com/).

## Funcionalidades:

- Pago vía webcheckout con codigo Qr.
- Reembolso de compras.
- Notas de orden automaticas.

## Versiones:

| Version | Funcionalidades |
| ------------ | ------------- |
| 1.0 | Primera versión del plugin, con Qr integrado. |
| 2.0 | Plugin reescrito, redirección a webcheckout implementado. |



